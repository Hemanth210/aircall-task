FROM node:14-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install 

COPY . .

EXPOSE 3000

# RUN chown -R node /usr/src/app
# USER node

CMD ["npm", "start"]
